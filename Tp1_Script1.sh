echo "nom de la machine $(hostname)"
echo "Adress IP: $( ifconfig | grep "inet " | grep -v 127.0.0.1 | cut -d\  -f2)"
echo "OS Name and Version $(sw_vers)"
echo "Last Reboot: $(who -b)"
softwareupdate -l
echo "RAM"
top -l 1 | head -n 10 | grep PhysMem | sed 's/, /n /g'
echo "Disk"
echo "Disk Used $(df -h | grep /dev/disk1s1 | cut -d' ' -f7)"
echo "Disk Avaible $(df -h | grep /dev/disk1s1 | cut -d' ' -f9)"
echo "8.8.8.8 ping response in ms: " 
ping -c 8 8.8.8.8 | grep 'round-trip' | cut -d'/' -f5 
speedtest | egrep -v '(ISP|Latency|Server|Packet Loss|Result URL)'
echo "List of Users:"
dscl . list /Users | grep -v _

