# Windows client TP1 de Yaniss Loisel B1A

# I/ Self-footprinting

### Host OS : 

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$    system_profiler SPSoftwareDataType
```
[...]
Computer Name: MacBook Pro de Yaniss
[...]
```

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$  sw_vers -productVersion
```
10.15.7
```
### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$  uname -a
```
Darwin MacBook-Pro-de-Yaniss.local 19.6.0 Darwin Kernel Version 19.6.0: Mon Aug 31 22:12:52 PDT 2020; root:xnu-6153.141.2~1/RELEASE_X86_64 x86_64
```
Grâce à la commannde uname -a j'ai pu retrouver l'architecture de mon processeur soit 64 bits

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$  system_profiler SPMemoryDataType
```[...]
 Size: 8 GB
          Type: LPDDR3
          Speed: 2133 MHz
          Status: OK
          Manufacturer: 0x802C
[...]
```

## Devices : 


### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$sysctl -n machdep.cpu.brand_string
```
Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz

```
Voici le modèle notre processeur gràce à la commande ci-dessus. Ici c'est un i7 de marque intel de génération 7 et fréquencé à 2.80GHz

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ sysctl hw.physicalcpu hw.logicalcpu
```hw.physicalcpu: 4
hw.logicalcpu: 8
```
On peut voir ici le nombre nos processeurs soit 4 physiques et donc 8 logiques

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ system_profiler SPDisplaysDataType
```
Graphics/Displays:

    Intel HD Graphics 630:

      Chipset Model: Intel HD Graphics 630
      [...]
      Radeon Pro 555:

      Chipset Model: Radeon Pro 555
      [...]
      
```
Voici les deux cartes graphiques de mon système

#### Disque dur


### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ diskutil info /dev/disk0 | grep 'Device / Media Name'
   
```
Device / Media Name:       APPLE SSD SM0256L
```

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ diskutil list  

##### Disk1
```#:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      APFS Container Scheme -                      +250.7 GB   disk1
                                 Physical Store disk0s2
   1:                APFS Volume Macintosh HD - Données  100.0 GB   disk1s1
   2:                APFS Volume Preboot                 82.4 MB    disk1s2
   3:                APFS Volume Recovery                528.9 MB   disk1s3
   4:                APFS Volume VM                      3.2 GB     disk1s4
   5:                APFS Volume Macintosh HD            11.2 GB    disk1s5
```
Ci dessus toutes les partitions de  mon disk1. On peut d'ailleurs y voir le sytème de fichier de chacune des partitions à savoir AFPS.
-Macintosh HD - Données: est un volume qui est totalement séparé ses autres pour éviter d'écraser accidentellement les fichiers essentiels au systèmes.C'est dans ce volume que sont stockés nos fichiers.
-Le PreBoot est un volume de partition qui gère les boots(les démarrages)
-Le Recovery est crée automatiquement lors du programme d'installation de MacOs autrement dit cest le volume du système de secours.
-Le volume VM (Virtual memory) recèle la sauvegarde du contexte de la RAM lors de la mise en veille du Mac.

## Users

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ dscl . list /Users | grep -v '_'
```
daemon
nobody
root
yanissloisel

```
Voici ci-dessus les 4 utilisateurs de ma machine

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ sudo whoami  
```
root
```
L'admin user de ma machine est donc root

## Processus

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ ps -ef
```
USER               PID  %CPU %MEM      VSZ    RSS   TT  STAT STARTED      TIME COMMAND
_windowserver      238   3,2  0,5  9152764  89192   ??  Ss   Mer11    62:12.50 /
yanissloisel     18609   2,8  0,8  9065692 132096   ??  S     6:03     6:40.78 /
 [...]
 _driverkit       21033   0,0  0,0  4802340   2096   ??  Ss    1:52     0:00.04 /
_driverkit       21032   0,0  0,0  4800292   2100   ??  Ss    1:52     0:00.02 /
```
Cette commande nous donne la liste de tous les processes en cours actuellement sur ma machine avec toutes leurs données.



### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ 

```
 696   ??  Ss     1:30.53 /System/Library/PrivateFrameworks/XprotectFramework.framework/Versions/A/XPCServices/XprotectService.xpc/Contents/MacOS/XprotectService
```
XProtect est un fichier de référence des différents malware sévissant sur Mac. macOS s'appuie sur ce répertoire pour prévenir l'utilisateur s'il s'apprête à ouvrir un fichier contenant un malware identifié
```
 309   ??  Ss    16:40.30 /usr/libexec/sysmond
```
Sysmond (system monitor daemon) est un process qui surveilles toutes les activités systèmes en arrière plan.
```
 1   ??  Ss    15:08.88 /sbin/launchd
```
Launchd est un daemon de gestion des services d'initialisation et de système d'exploitation
```
   116   ??  Ss     5:19.33 /System/Library/CoreServices/powerd.bundle/powerd
```
Ce démon particulier, powerd, gère votre consommation d'énergie.
Lorsque l'appareil se met en veille après avoir été inactif, c'est Powerd qui le fait.
```
   151   ??  Ss     1:56.11 /usr/libexec/dasd

```

Duet activity scheduler (DAS) maintient un liste notée des activités d'arrère plan qui se compose généralement de plus de 70 éléments. Périodiquement, il remet en mémoire chaque élément de qa liste, en fonction de divers critères tels que la date à laquelle il doit être exécuté, c'est à dire que l'heure de l'horloge est maintenant comprise dans la période pendant laquelle la planification centralisé des tâches (CTS) a calculé qu'il devrait ensuite

### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ ps -U root
```
  PID   TT  STAT      TIME COMMAND
    1   ??  Ss    15:08.88 /sbin/launchd
  102   ??  Ss     0:32.30 /usr/sbin/syslogd
  103   ??  Ss     1:23.50 /usr/libexec/UserEventAgent (System)
  105   ??  Ss     0:00.01 /Library/PrivilegedHelperTools/com.crystalidea.macsfancontrol.smcwrite
  107   ??  Ss     0:06.46 /System/Library/PrivateFrameworks/Uninstall.framework/Resources/uninstalld
  108   ??  Ss     1:32.87 /usr/libexec/kextd
  109   ??  Ss     5:46.34 /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/FSEvents.framework/Versions/A/Support/fseventsd
  110   ??  Ss     0:16.19 /System/Library/PrivateFrameworks/MediaRemote.framework/Support/mediaremoted
  113   ??  Ss     7:41.84 /usr/sbin/systemstats --daemon
  114   ??  Ss     7:40.68 /usr/libexec/configd
  115   ??  Ss     0:00.02 endpointsecurityd
  116   ??  Ss     5:19.33 /System/Library/CoreServices/powerd.bundle/powerd
  120   ??  Ss     3:36.80 /usr/libexec/logd
  121   ??  Ss     0:00.78 /usr/libexec/keybagd -t 15
  124   ??  Ss     0:07.03 /usr/libexec/watchdogd
  128   ??  Ss     5:52.81 /System/Library/Frameworks/CoreServices.framework/Frameworks/Metadata.framework/Support/mds
  130   ??  Ss     1:08.19 /usr/libexec/diskarbitrationd
  133   ??  Ss     1:12.56 /usr/libexec/coreduetd
  137   ??  Ss     3:38.71 /usr/libexec/opendirectoryd
  138   ??  Ss     0:56.78 /System/Library/PrivateFrameworks/ApplePushService.framework/apsd
  139   ??  Ss     8:39.95 /System/Library/CoreServices/launchservicesd
  142   ??  Ss     1:31.78 /usr/sbin/securityd -i
  143   ??  Ss     0:00.05 auditd -l
  148   ??  Ss     0:00.04 autofsd
  151   ??  Ss     1:56.11 /usr/libexec/dasd
  155   ??  Ss     0:00.28 /System/Library/CoreServices/logind
  156   ??  Ss     0:01.90 /System/Library/PrivateFrameworks/GenerationalStorage.framework/Versions/A/Support/revisiond
  157   ??  Ss     0:00.02 /usr/sbin/KernelEventAgent
  159   ??  Ss     5:11.99 /usr/sbin/bluetoothd
  162   ??  Ss     1:20.78 /usr/libexec/corebrightnessd --launchd
  163   ??  Ss     0:41.47 /usr/libexec/AirPlayXPCHelper
  164   ??  Ss     0:10.30 /usr/libexec/amfid
  165   ??  Ss     2:36.39 /usr/sbin/notifyd
  167   ??  Ss    19:31.84 /usr/libexec/syspolicyd
  168   ??  Ss     0:41.22 /usr/sbin/cfprefsd daemon
  169   ??  Ss     0:00.51 aslmanager
  170   ??  Ss     0:26.12 /System/Library/PrivateFrameworks/TCC.framework/Resources/tccd system
  187   ??  Ss     2:27.19 /System/Library/CoreServices/loginwindow.app/Contents/MacOS/loginwindow console
  194   ??  Ss     0:06.40 /System/Library/Frameworks/Security.framework/Versions/A/XPCServices/authd.xpc/Contents/MacOS/authd
  197   ??  Ss     0:08.28 /System/Library/CoreServices/coreservicesd
  199   ??  Ss     1:12.87 /System/Library/PrivateFrameworks/CoreDuetContext.framework/Resources/contextstored
  204   ??  Ss     0:02.38 /usr/libexec/nehelper
  206   ??  Ss     2:22.78 /usr/libexec/trustd
  210   ??  Ss     0:02.31 /usr/libexec/searchpartyd
  211   ??  Ss    13:09.96 /usr/libexec/airportd
  238   ??  Ss   135:31.28 /System/Library/PrivateFrameworks/SkyLight.framework/Resources/WindowServer -daemon
  240   ??  Ss     0:27.04 /System/Library/PrivateFrameworks/WirelessDiagnostics.framework/Support/awdd
  254   ??  Ss     0:50.79 /usr/libexec/mobileassetd
  255   ??  Ss     0:09.30 /usr/sbin/mDNSResponderHelper
  274   ??  Ss     0:00.14 /System/Library/Frameworks/AudioToolbox.framework/AudioComponentRegistrar -daemon
  275   ??  Ss     0:17.30 /usr/libexec/runningboardd
  281   ??  Ss     0:17.77 /usr/libexec/lsd runAsRoot
  282   ??  Ss     0:02.32 /System/Library/Frameworks/Security.framework/Versions/A/XPCServices/com.apple.CodeSigningHelper.xpc/Contents/MacOS/com.apple.CodeSigningHelper
  283   ??  Ss     0:02.67 /System/Library/CoreServices/backupd.bundle/Contents/Resources/backupd-helper -launchd
  285   ??  Ss     8:07.21 /System/Library/Frameworks/CoreServices.framework/Frameworks/Metadata.framework/Versions/A/Support/mds_stores
  291   ??  Ss     0:00.39 /usr/libexec/secinitd
  293   ??  Ss     0:02.11 /usr/libexec/diskmanagementd
  296   ??  Ss    13:02.17 /usr/libexec/TouchBarServer
  298   ??  Ss     0:00.12 /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/CVMServer
  299   ??  Ss     0:00.13 /usr/libexec/colorsync.displayservices
  300   ??  Ss     0:00.03 /usr/libexec/colorsyncd
  301   ??  Ss     0:00.48 /System/Library/CryptoTokenKit/com.apple.ifdreader.slotd/Contents/MacOS/com.apple.ifdreader
  302   ??  Ss     0:01.10 /usr/libexec/apfsd
  303   ??  Ss     0:03.15 /usr/libexec/eoshostd
  304   ??  Ss     0:23.39 /usr/libexec/usbd
  305   ??  Ss     0:00.58 /usr/libexec/thermald
  307   ??  Ss     0:11.36 /usr/libexec/biometrickitd --launchd
  308   ??  Ss     0:01.31 /usr/libexec/xartstorageremoted
  309   ??  Ss    16:40.30 /usr/libexec/sysmond
  313   ??  Ss     0:00.02 /usr/libexec/bootinstalld
  314   ??  S      0:07.72 /System/Library/PrivateFrameworks/ViewBridge.framework/Versions/A/XPCServices/ViewBridgeAuxiliary.xpc/Contents/MacOS/ViewBridgeAuxiliary
  315   ??  S      0:00.91 /usr/sbin/distnoted agent
  317   ??  Ss     0:00.36 /System/Library/PrivateFrameworks/EmbeddedOSInstall.framework/Versions/A/XPCServices/EmbeddedOSInstallService.xpc/Contents/MacOS/EmbeddedOSInstallService
  323   ??  S      0:03.61 /usr/sbin/systemstats --logger-helper /private/var/db/systemstats
  324   ??  Ss     0:00.34 /System/Library/PrivateFrameworks/AccountPolicy.framework/XPCServices/com.apple.AccountPolicyHelper.xpc/Contents/MacOS/com.apple.AccountPolicyHelper
  328   ??  Ss     0:00.28 /System/Library/Frameworks/GSS.framework/Helpers/GSSCred
  331   ??  Ss     0:02.34 /System/Library/Frameworks/LocalAuthentication.framework/Support/coreauthd
  334   ??  Ss     0:00.85 /usr/libexec/securityd_service
  335   ??  Ss     0:00.01 /System/Library/Frameworks/CoreMediaIO.framework/Versions/A/XPCServices/com.apple.cmio.registerassistantservice.xpc/Contents/MacOS/com.apple.cmio.registerassistantservice
  339   ??  Ss     0:00.25 /usr/libexec/smd
  340   ??  Ss     0:00.96 /usr/libexec/biokitaggdd
  341   ??  Ss     0:04.76 /usr/sbin/spindump
  342   ??  Ss     3:20.44 /System/Library/CoreServices/SubmitDiagInfo server-init
  368   ??  Ss     0:02.35 /usr/sbin/systemsoundserverd
  401   ??  Ss     0:05.65 /usr/sbin/filecoordinationd
  411   ??  Ss     0:04.88 /usr/sbin/WirelessRadioManagerd
  442   ??  Ss     0:11.87 /usr/libexec/wifivelocityd
  474   ??  Ss     0:03.80 /usr/libexec/findmydeviced
  483   ??  Ss     0:12.48 /System/Library/PrivateFrameworks/AmbientDisplay.framework/Versions/A/XPCServices/com.apple.AmbientDisplayAgent.xpc/Contents/MacOS/com.apple.AmbientDisplayAgent
  523   ??  Ss     0:00.20 /System/Library/Frameworks/CryptoTokenKit.framework/ctkahp.bundle/Contents/MacOS/ctkahp -d
  533   ??  Ss     0:02.81 /usr/libexec/taskgated
  566   ??  Ss     0:00.01 /System/Library/PrivateFrameworks/FindMyMac.framework/Resources/FindMyMacd
  570   ??  Ss     0:00.05 /System/Library/CoreServices/CrashReporterSupportHelper server-init
  594   ??  Ss     0:00.12 /System/Library/PrivateFrameworks/AssetCacheServicesExtensions.framework/XPCServices/AssetCacheTetheratorService.xpc/Contents/MacOS/AssetCacheTetheratorService
  607   ??  Ss     0:00.38 /System/Library/Frameworks/SystemExtensions.framework/Versions/A/Helpers/sysextd
  614   ??  Ss     0:45.41 /usr/libexec/dprivacyd
  638   ??  Ss     0:00.48 /System/Library/CoreServices/Software Update.app/Contents/Resources/suhelperd
  639   ??  Ss     0:02.04 /System/Library/PrivateFrameworks/AuthKit.framework/Versions/A/Support/akd
  696   ??  Ss     1:30.53 /System/Library/PrivateFrameworks/XprotectFramework.framework/Versions/A/XPCServices/XprotectService.xpc/Contents/MacOS/XprotectService
  725   ??  Ss     1:11.73 /System/Library/PrivateFrameworks/StorageKit.framework/Resources/storagekitd
  753   ??  Ss     0:03.30 /usr/libexec/rtcreportingd
  791   ??  Ss     0:03.18 /System/Library/PrivateFrameworks/CacheDelete.framework/deleted_helper
  796   ??  Ss    24:43.90 /System/Library/PrivateFrameworks/PackageKit.framework/Resources/installd
  797   ??  Ss     0:11.75 /System/Library/PrivateFrameworks/CoreSymbolication.framework/coresymbolicationd
  798   ??  Ss     0:01.04 /System/Library/PrivateFrameworks/PackageKit.framework/Resources/system_installd
  811   ??  Ss     0:02.05 /System/Library/CoreServices/iconservicesagent runAsRoot
 2282   ??  S      0:22.83 /System/Library/PrivateFrameworks/GeoServices.framework/Versions/A/XPCServices/com.apple.geod.xpc/Contents/MacOS/com.apple.geod
 2283   ??  S      0:00.33 /usr/libexec/secinitd
 2496   ??  Ss     0:00.23 /System/Library/Frameworks/AudioToolbox.framework/XPCServices/CAReportingService.xpc/Contents/MacOS/CAReportingService
 2854   ??  Ss     0:00.48 /System/Library/PrivateFrameworks/PerformanceAnalysis.framework/Versions/A/XPCServices/com.apple.PerformanceAnalysis.animationperfd.xpc/Contents/MacOS/com.apple.PerformanceAnalysis.animationperfd
 3477   ??  Ss     0:00.30 /System/Library/PrivateFrameworks/Noticeboard.framework/Versions/A/Resources/nbstated
 4055   ??  Ss     0:00.09 /usr/libexec/mobileactivationd
 4650   ??  Ss     0:00.46 /System/Library/PrivateFrameworks/CoreAccessories.framework/Support/accessoryd
 4893   ??  Ss     0:00.15 /usr/libexec/tzd
 5038   ??  Ss     0:00.39 /usr/libexec/microstackshot
 5039   ??  SNs    0:00.02 /usr/libexec/periodic-wrapper daily
 5108   ??  Ss     0:00.25 /usr/bin/sysdiagnose
 6544   ??  Ss     0:00.04 /System/Library/CoreServices/ReportCrash daemon
13661   ??  Ss     0:00.01 /usr/sbin/aslmanager -s /var/log/eventmonitor
14342   ??  Ss     0:03.42 /System/Library/PrivateFrameworks/AppStoreDaemon.framework/Support/appstored
14477   ??  SNs    0:00.01 /usr/libexec/periodic-wrapper weekly
15118   ??  Ss     0:00.02 /usr/libexec/applessdstatistics
18573   ??  Ss     0:00.01 /System/Library/Frameworks/Security.framework/authtrampoline
18631   ??  Ss     0:00.66 /Library/PrivilegedHelperTools/com.adobe.acc.installer.v2
19109   ??  Ss     0:00.07 /System/Library/Frameworks/Metal.framework/Versions/A/XPCServices/MTLCompilerService.xpc/Contents/MacOS/MTLCompilerService
20191   ??  Ss     0:00.15 /System/Library/PrivateFrameworks/SystemAdministration.framework/XPCServices/writeconfig.xpc/Contents/MacOS/writeconfig
20823   ??  Ss     0:00.10 /System/Library/PrivateFrameworks/DiskImages.framework/Resources/hdiejectd
22041   ??  S      0:02.31 /System/Library/PrivateFrameworks/PackageKit.framework/Versions/A/XPCServices/package_script_service.xpc/Contents/MacOS/package_script_service
48657   ??  Ss     0:00.10 /System/Library/CoreServices/sharedfilelistd
66748   ??  Ss     0:00.03 /System/Library/PrivateFrameworks/AssetCacheServicesExtensions.framework/XPCServices/AssetCacheManagerService.xpc/Contents/MacOS/AssetCacheManagerService
72729   ??  Ss     0:36.82 /usr/libexec/PerfPowerServices
77250   ??  Ss     0:00.92 /usr/sbin/ocspd
77817   ??  Ss     0:00.03 /private/var/db/com.apple.xpc.roleaccountd.staging/com.apple.activitymonitor.helper.16777220.1152921500312473713.xpc/Contents/MacOS/com.apple.activitymonitor.helper
77818   ??  Ss     0:00.04 /usr/libexec/xpcroleaccountd -launchd
```
Cette commande nous donne tous les processes reéalisés par l'user admin


## Network


### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ networksetup -listallnetworkservices
```
Wi-Fi
```
Carte correpondant à la connexion WIFI
```
Bluetooth PAN
```
Carte correspondant à la connexion Bluetooth
```
iPhone
```
Carte correspondant au partage de connexion avec mon smartphone
```
USB 10/100/1000 LAN
```
Carte correspondant à la connexion Ethernet
```
Thunerbolt Bridge
```

Carte qui permet d'utiliser un câble Thunderbolt pour relier deux Mac et créer un réseau



### MacBook-Pro-de-Yaniss:tp1_workstation yanissloisel$ netstat 

```
tcp4       0      0  10.33.3.8.55821        ec2-52-30-157-37.https ESTABLISHED
tcp4       0      0  10.33.3.8.55820        ec2-18-179-225-2.https ESTABLISHED
tcp4       0      0  10.33.3.8.55817        ec2-18-179-138-2.https ESTABLISHED
tcp4       0      0  10.33.3.8.55815        ec2-52-54-178-15.https ESTABLISHED
tcp4       0      0  10.33.3.8.55811        uklon5-vip-bx-00.https ESTABLISHED
tcp4       0      0  10.33.3.8.55801        104.17.78.107.https    ESTABLISHED
tcp4       0      0  10.33.3.8.55792        104.27.162.84.https    ESTABLISHED
tcp4       0      0  10.33.3.8.55791        104.27.162.84.https    ESTABLISHED
tcp4       0      0  10.33.3.8.55789        ec2-15-236-69-95.https ESTABLISHED
tcp4       0      0  10.33.3.8.55763        17.248.144.91.https    ESTABLISHED
tcp4       0      0  10.33.3.8.55742        stackoverflow.co.https ESTABLISHED
tcp4       0      0  10.33.3.8.55719        17.248.144.91.https    ESTABLISHED
tcp4       0      0  10.33.3.8.55718        17.248.144.81.https    ESTABLISHED
tcp4       0      0  10.33.3.8.55699        ec2-3-217-71-63..https ESTABLISHED
tcp4       0      0  10.33.3.8.55448        dzr-viplb-01.ig-.https ESTABLISHED
tcp4       0      0  10.33.3.8.55272        40.67.251.132.https    ESTABLISHED
tcp4       0      0  10.33.3.8.55244        162.254.196.68.27039   ESTABLISHED
tcp4       0      0  10.33.3.8.55239        52.97.241.178.https    ESTABLISHED
tcp4       0      0  10.33.3.8.55236        17.42.251.32.imaps     ESTABLISHED
tcp4       0      0  10.33.3.8.55226        bt3.api.mega.co..https ESTABLISHED
tcp4       0      0  10.33.3.8.55215        blm-pm-01.ig-1.n.https ESTABLISHED
tcp4       0      0  10.33.3.8.55200        17.42.251.32.imaps     ESTABLISHED
tcp4       0      0  10.33.3.8.55198        17.57.146.165.https    ESTABLISHED
tcp6       0      0  fe80::aede:48ff:.55190 fe80::aede:48ff:.61000 ESTABLISHED
tcp4       0      0  localhost.57669        localhost.54456        ESTABLISHED
tcp4       0      0  localhost.54456        localhost.57669        ESTABLISHED
tcp6       0      0  fe80::aede:48ff:.54426 fe80::aede:48ff:.52032 ESTABLISHED
tcp6       0      0  macbook-pro-de-y.black fe80::86a6:202e:.36185 ESTABLISHED
tcp6       0      0  macbook-pro-de-y.1024  fe80::86a6:202e:.1024  ESTABLISHED
tcp4       0      0  192.168.1.36.56164     40.67.251.132.https    CLOSE_WAIT 
tcp4       0      0  192.168.1.71.51622     40.67.251.132.https    ESTABLISHED
tcp4       0      0  10.33.3.8.55727        fra16s25-in-f4.1.https TIME_WAIT  
tcp4       0      0  10.33.3.8.55734        mil07s09-in-f14..https TIME_WAIT  
tcp4       0      0  10.33.3.8.55802        par10s27-in-f195.http  TIME_WAIT  
udp4       0      0  *.53313                *.*                               
udp6       0      0  *.65268                *.*                               
udp4       0      0  *.65268                *.*                               
udp6       0      0  *.56539                *.*                               
udp4       0      0  *.56539                *.*                               
udp6       0      0  *.61443                *.*                               
udp4       0      0  *.61443                *.*                               
udp6       0      0  *.57964                *.*                               
udp4       0      0  *.57964                *.*                               
udp6       0      0  *.52831                *.*                               
udp4       0      0  *.52831                *.*                               
udp6       0      0  *.59249                *.*                               
udp4       0      0  *.59249                *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.27036                *.*                               
udp4       0      0  *.56718                *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.53962                *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp46      0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp6       0      0  *.mdns                 *.*                               
udp4       0      0  *.mdns                 *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.*                    *.*                               
udp4       0      0  *.netbios-dgm          *.*                               
udp4       0      0  *.netbios-ns           *.*          


```

### macbook-pro-de-yaniss:~ yanissloisel$ lsof -i -P | grep -i "listen"
```
rapportd    390 yanissloisel    4u  IPv4 0xef3342a8d1f0bd5b      0t0  TCP *:55184 (LISTEN)
rapportd    390 yanissloisel    5u  IPv6 0xef3342a8af61a82b      0t0  TCP *:55184 (LISTEN)
MEGAclien   524 yanissloisel   34u  IPv4 0xef3342a8b5d9a37b      0t0  TCP localhost:6341 (LISTEN)
MEGAclien   524 yanissloisel   42u  IPv4 0xef3342a8da45afbb      0t0  TCP localhost:6342 (LISTEN)
Adobe\x20 18600 yanissloisel    9u  IPv4 0xef3342a8b6c20afb      0t0  TCP localhost:15292 (LISTEN)
Adobe\x20 18600 yanissloisel   12u  IPv4 0xef3342a8b9628d5b      0t0  TCP localhost:16494 (LISTEN)
Adobe\x20 18600 yanissloisel   14u  IPv4 0xef3342a8b8c24d5b      0t0  TCP localhost:15393 (LISTEN)
node      72463 yanissloisel   33u  IPv4 0xef3342a8ca88ad5b      0t0  TCP localhost:57669 (LISTEN)
node      72463 yanissloisel   35u  IPv4 0xef3342a8b9f6737b      0t0  TCP localhost:57670 (LISTEN)
node      72463 yanissloisel   38u  IPv4 0xef3342a8b962aafb      0t0  TCP localhost:45623 (LISTEN)
node      72463 yanissloisel   39u  IPv4 0xef3342a8d1f0b37b      0t0  TCP localhost:51889 (LISTEN)
steam_osx 73750 yanissloisel   18u  IPv4 0xef3342a8d738b11b      0t0  TCP localhost:57343 (LISTEN)
steam_osx 73750 yanissloisel   26u  IPv4 0xef3342a8d738c4db      0t0  TCP localhost:27060 (LISTEN)
steam_osx 73750 yanissloisel   75u  IPv4 0xef3342a8d732b37b      0t0  TCP *:27036 (LISTEN)
```
Je n'ai pas trouvé de commandes exactes mais gràce a celles-ci dessus on retrouve ;
```
rapportd    390 yanissloisel    4u  IPv4 0xef3342a8d1f0bd5b      0t0  TCP *:55184 (LISTEN)
```
-> correspond au port TCP ouvert qui correspond au rapporttd
```
steam_osx 73750 yanissloisel   18u  IPv4 0xef3342a8d738b11b      0t0  TCP localhost:57343 (LISTEN)
```
-> correspond au port TCP ouvert qui correspond au logiciel Steam
```
MEGAclien   524 yanissloisel   42u  IPv4 0xef3342a8da45afbb      0t0  TCP localhost:6342 (LISTEN)
```
-> correspond au port TCP ouvert qui correspond à l'application MEGA download
```
Adobe\x20 18600 yanissloisel    9u  IPv4 0xef3342a8b6c20afb      0t0  TCP localhost:15292 (LISTEN)
```
-> correspond au port TCP ouvert qui correspond à l'application Adobe 

# II/Scripting

Voir Tp1_Script1.sh et Tp1_Script2.sh envoyés à part dans le même dossier que le fichier markdown.



## Gestion des Softs 


### Expliquer l'intérêt d'un gestionnaire de paquets

Un gestionnaire de paquets est un (ou plusieurs) outil(s) automatisant le processus d'installation, désinstallation, mise à jour de logiciels installés sur un système informatique. Ils permettent de mettre à disposition simplement des milliers de paquetages lors d'une installation standard.Tous les logiciels (les paquets) sont centralisés dans un seul et même serveur (le dépôt) :
* plus besoin de parcourir le Web pour trouver les fichiers d’installation de tel ou tel logiciel, tous se trouvent au même endroit ! On va enfin pouvoir installer et mettre à jour plusieurs logiciels avec une seule commande.
* Tous les logiciels sont dépourvus de spywares et malwares en tout genre : toutes les cases qui installaient des logiciels non désirés n’existent plus.
* Les dépendances sont automatiquement installées : si un logiciel a besoin d’un programme (ex. : Microsoft Visual C++ 2010 Redistributable Package) pour fonctionner, le gestionnaire va l’installer automatiquement.

### Utiliser un gestionnaire de paquet propres à votre OS pour
#### Lister tous les paquets installés 
#### MacBook-Pro-de-Yaniss:~ yanissloisel$ brew list
```
jq		oniguruma	speedtest

```
On voit ici que j'ai 3 gros paquets installés.
Oniguruma est une bibliothèque d'expressions régulières sous licence BSD qui prend en charge une variété d'encodages de caractères
JQ nous permet un à la manière de grep ou de awk de séléctionner de modifier ou encore de séléctionner des données structurés.
Speed Test est un paquet faisant référence a une interface de ligne de commande permettant de mesurer sa connexion Internet
### Oniguruma
#### MacBook-Pro-de-Yaniss:~ yanissloisel$ brew list oniguruma
```
/usr/local/Cellar/oniguruma/6.9.5-rev1/bin/onig-config
/usr/local/Cellar/oniguruma/6.9.5-rev1/include/ (2 files)
/usr/local/Cellar/oniguruma/6.9.5-rev1/lib/libonig.5.dylib
/usr/local/Cellar/oniguruma/6.9.5-rev1/lib/pkgconfig/oniguruma.pc
/usr/local/Cellar/oniguruma/6.9.5-rev1/lib/ (2 other files)
```
On voit ici que ce paquet contient différents sous-paquets
### JQ
#### MacBook-Pro-de-Yaniss:~ yanissloisel$ brew list jq

```
/usr/local/Cellar/jq/1.6/bin/jq
/usr/local/Cellar/jq/1.6/include/ (2 files)
/usr/local/Cellar/jq/1.6/lib/libjq.1.dylib
/usr/local/Cellar/jq/1.6/lib/ (2 other files)
/usr/local/Cellar/jq/1.6/share/doc/ (4 files)
/usr/local/Cellar/jq/1.6/share/man/man1/jq.1
```

### Speedtest
#### MacBook-Pro-de-Yaniss:~ yanissloisel$ brew list speedtest
```
/usr/local/Cellar/speedtest/1.0.0/bin/speedtest
/usr/local/Cellar/speedtest/1.0.0/share/man/man5/speedtest.5
```

### MacBook-Pro-de-Yaniss:~ yanissloisel$ brew info speedtest | grep From
```
From: https://github.com/teamookla/homebrew-speedtest/blob/HEAD/speedtest.rb
```
Ce paquet nous a été delivré depuis l'adresse ci-dessus 

### MacBook-Pro-de-Yaniss:~ yanissloisel$ brew info jq | grep From
```
From: https://github.com/Homebrew/homebrew-core/blob/HEAD/Formula/jq.rb
```
Ce paquet nous a été delivré depuis l'adresse ci-dessus 

### MacBook-Pro-de-Yaniss:~ yanissloisel$ brew info oniguruma | grep From
```
From: https://github.com/Homebrew/homebrew-core/blob/HEAD/Formula/oniguruma.rb
```
Ce paquet nous a été delivré depuis l'adresse ci-dessus 


Après avoir réealisé une connexion SSH entre ma VM et mon Mac. 
Je vérifie que la connexion est bien en place avec la commande ping. 
```
macbook-pro-de-yaniss:/ yanissloisel$ ping -c 2 192.168.120.3
PING 192.168.120.3 (192.168.120.3): 56 data bytes
64 bytes from 192.168.120.3: icmp_seq=0 ttl=64 time=0.410 ms
64 bytes from 192.168.120.3: icmp_seq=1 ttl=64 time=0.427 ms
```
En pingant l'IP de ma vm depuis mon Mac on remarque qu'ils communiquent bien entre eux car la commande ping me renvoie bien le temps pour laquelle la requête a fait l'aller retour entre les deux .

# IV/Partage de fichiers
 
- J'installe les paquets NFS sur mon Mac(serveur) et ma vm(client) grâce à la commande 
```
yum install nfs-utils
```
- Pour monter un serveur NFS sur mon Mac je crée un dossier Projects dans mon dossier home(yaniss loisel) afin d'éviter que le TCC n'autorise par le partage. Le TCC(Transparency, Consent and control) est le modèle de permission de chez Apple, celui ci  autorise ou n'autorise pas certaines actions pour protéger les données de l'User. Dans notre cas il n'autorise le partage de fichier via nfs uniquement sur un chemin spécial. En effet notre dossier que l'on voudra partager devra avoir le chemin suivant ```~/Projetcs ```, ainsi ce chemin n'engendrera pas de problèmes avec les politiques TCC de mon macOs.

- Je créer un fichier exports dans mon dossier etc accessible sur mon terminal. Dans ce fichier j'y sauvegarde une commande qui aura     pour but de configurer le partage.
```
/Users/yanissloisel/Projects -rw -mapall=yanissloisel 192.168.120.3
```
/Users/yanissloisel/Projects est le fichier que je veux partager et que je veux rendre accessible depuis ma vm. -rw  sert à donner à l'ordinateur client un accès en lecture et en écriture au volume. Mapall=yanissloisel est l'user de mon Mac, le partage donnera donc au client les mêmes droits que cet utilisateur. L'adresse IP 192.168.120.3 est l'IP de la carte réseau de ma VM qui a été nécessaire à la connexion SSH entre mon Mac et ma VM.


- Après avoir enregistré et fermé mon fichier exports je démarre le serveur NFS pour rendre les parts disponibles au client.
```
macbook-pro-de-yaniss:/ yanissloisel$ sudo nfsd start
```
- Avec nfsd status je vérifie si mon serveur à bien demarré.
```
macbook-pro-de-yaniss:/ yanissloisel$ nfsd status
nfsd service is enabled
nfsd is running (pid 95428, 8 threads)
```


Maintenant que le serveur hôte est configuré et que le serveur nfs fonctionne sur mon Mac. Je peux donc configuré mon client.

- Je crée donc mon répertoire de montage sur ma vm.
Le répertoire s'apelle Import et il se trouve dans mon dossier mnt.
C'est dans le dossier Import que je pourrais avoir accès  aux fichiers partagés de mon Mac.
Dans la VM  je rentre la commande suivante 
```
[root@localhost Import]# mount -t nfs 192.168.120.1:/Users/yanissloisel/Projects /mnt/Import
mount.nfs: /mnt/Import is busy or already mounted
```
192.168.120.1 est l'IP de la carte réseau de mon Mac qui a été nécessaire à la connexion SSH entre mon ordinateur et ma VM. /Users/yanissloisel/Projects est le dossier contenant les fichiers que je souhaite partagé depuis mon ordinateur. /mnt/Import est le dossier se trouvant dans ma VM et sur lequel je souhaite avoir accès a mon dossier partagé depuis mon ordinateur.
Ici on voit bien que dossier de montage est déja monté a l'emplacement /mnt/Import.

Ainsi mon partage de fichiers depuis mon Mac(serveur) vers ma VM(client) doit fonctionner. Je peux essayer de vérifier si mon partage est bien actif.

Je vais donc créer un fichier TestNFS depuis ma VM dans mon répertoire Import. Si ce fichier est visible depuis mon Mac dans mon dossier ~/Projects  alors cela signifiera que mon partage a bien fonctionné.
```
[root@localhost Import]# pwd
/mnt/Import
[root@localhost Import]# touch TestNFS
```

Depuis mon Mac je vérifie si ce fichier est présent dans mon dossier home.
```
macbook-pro-de-yaniss:Projects yanissloisel$ pwd
/Users/yanissloisel/Projects
macbook-pro-de-yaniss:Projects yanissloisel$ ls
TestNFS
```

Ainsi on remarque que mon partage de fichiers NFS est actif.
```
[root@localhost Import]# df -h
Sys. de fichiers                           Taille Utilisé Dispo Uti% Monté sur
devtmpfs                                     908M       0  908M   0% /dev
tmpfs                                        919M       0  919M   0% /dev/shm
tmpfs                                        919M    8,6M  911M   1% /run
tmpfs                                        919M       0  919M   0% /sys/fs/cgroup
/dev/mapper/centos-root                      6,2G    1,3G  5,0G  21% /
/dev/sda1                                   1014M    150M  865M  15% /boot
tmpfs                                        184M       0  184M   0% /run/user/0
192.168.120.1:/Users/yanissloisel/Projects   234G    112G  108G  52% /mnt/Import
``` 
De plus grâce à la commande df -h qui nous permet d'afficher les partitions alloués sur notre disque, on remarque que le fichier /mnt/Import a bien une partition allouée sur le disque de ma VM correspondant aux données partagés depuis mon Mac.























































